package com.fjb.config.im;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Description:聊天信息
 * @author hemiao
 * @time:2020年5月31日 下午7:15:46
 */
public class ChatMsg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主账号id
	 */
	private Integer mainUserId;
	
	/**
	 * 用户id
	 */
	private Integer userId;
	
	/**
	 * 发送者的用户id	
	 */
	private String sendId;		
	
	/**
	 * 接受者的用户id
	 */
	private String receiveId;
	
	/**
	 * 聊天内容
	 */
	private String msgContent;
	
	/**
	 * 消息发送时间
	 */
	private Date sendTime;
	
	/**
	 * 用于消息的签收
	 */
	private String msgId;

	public Integer getMainUserId() {
		return mainUserId;
	}

	public void setMainUserId(Integer mainUserId) {
		this.mainUserId = mainUserId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSendId() {
		return sendId;
	}

	public void setSendId(String sendId) {
		this.sendId = sendId;
	}

	public String getReceiveId() {
		return receiveId;
	}

	public void setReceiveId(String receiveId) {
		this.receiveId = receiveId;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

}
