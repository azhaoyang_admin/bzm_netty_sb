package com.fjb.pojo.im.vo;

import com.fjb.pojo.im.ImChatFriendRequestLogs;

/**
 * @Description:TODO
 * @author hemiao
 * @time:2020年6月3日 下午7:24:16
 */
public class ImChatFriendRequestLogsVo extends ImChatFriendRequestLogs{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8156847285263720109L;

	private String nickname;
	
	private String gender;
	
	private String phone;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
