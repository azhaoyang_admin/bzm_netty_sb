package com.fjb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
// 扫描mybatis mapper包路径
@MapperScan(basePackages = "com.fjb.mapper")
//扫描 所有需要的包, 包含一些自用的工具类包 所在的路径
@ComponentScan(basePackages= {"com.fjb"})
public class FjbDemoWebApplication {
	
	@Bean
	public SpringBeanUtil getSpringBeanUtil() {
		return new SpringBeanUtil();
	}
	
    public static void main(String[] args) {
        SpringApplication.run(FjbDemoWebApplication.class, args);
    }

}
